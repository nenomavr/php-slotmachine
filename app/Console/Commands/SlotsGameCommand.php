<?php 

//namespace Laravel\Lumen\Console\Commands;
namespace App\Console\Commands;

use Illuminate\Console\Command;
use stdClass;
use Exception;

class SlotsGameCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'slots';

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "slots";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Slots game for VideoSlots assignment";

    protected $boardSymbols = [];
    protected $jsonResult;

    protected function initVariables() {
        /* Symbols orders added into the board */ 
        define("MAPPED_CELLS_ON_BOARD", [0, 3, 6, 9, 12, 1, 4, 7, 10, 13, 2, 5, 8, 11, 14]);

        /* Available Symbols in the game */
        define("AVAILABLE_SYMBOLS", ['9', '10', 'J', 'Q', 'K', 'A', 'CAT', 'DOG', 'MONKEY', 'BIRD']);

        /* Posible paylines in the game */
        define("PAYLINES", [
                        [0, 3, 6, 9, 12],
                        [1, 4, 7, 10, 13],
                        [2, 5, 8, 11, 14],
                        [0, 4, 8, 10, 12],
                        [2, 4, 6, 10, 14]
                    ]);

        /* Amount bet by the user (1€, 100cents) */
        define("BET_AMOUNT", 100);

        $this->jsonResult = $this->generateEmptyJsonResult();
    }

    protected function getRandomSymbol() {
        return AVAILABLE_SYMBOLS[array_rand(AVAILABLE_SYMBOLS)];
    }

    protected function generateEmptyJsonResult() {
        $json = new stdClass();
        $json->board = [];
        $json->paylines = [];
        $json->bet_amount = BET_AMOUNT;
        $json->total_win = 0;

        return $json;
    }

    protected function initBoardSymbols() {
        for ($i = 0; $i < 15; $i++) {
            $symbol = $this->getRandomSymbol();

            array_push($this->boardSymbols, $symbol);
        }

        $this->jsonResult->board = $this->boardSymbols;
    }

    protected function verifyPaylines() {
        forEach(PAYLINES as $payline) {
            $firstSymbol = null;
            $matchedSymbols = 1;
            $shouldCheckNextSymbol = true;

            forEach($payline as $position) {
                if (!$shouldCheckNextSymbol) {
                    break;
                }

                /* Symbol index that we are looking for */
                $index = array_search($position, MAPPED_CELLS_ON_BOARD);

                /* First time */
                if ($firstSymbol === null) {
                    $firstSymbol = $this->boardSymbols[$index];
                } else {
                    $currentSymbol = $this->boardSymbols[$index];

                    if ($firstSymbol == $currentSymbol) {
                        $matchedSymbols++;
                    } else {
                        $shouldCheckNextSymbol = false;
                    }
                }
            }

            $this->pushResults($payline, $matchedSymbols);
        }
    }

    protected function calculatePrize($matchedSymbols) {
        $win_amount = 0;

        switch ($matchedSymbols) {
            case 3: /* 3 matched symbols pays out 20% of bet */
                $win_amount = BET_AMOUNT * 0.20;
                break;
            case 4: /* 4 matched symbols pays out 200% of bet */
                $win_amount = BET_AMOUNT * 2;
                break;
            case 5: /* 5 matched symbols pays out 1000% of bet */
                $win_amount = BET_AMOUNT * 10;
                break;
            default:
                throw new Exception();
                break;
        }

        return $win_amount;
    }


    protected function pushResults($payline, $matchedSymbols) {
        /* Only publish paylines with 3 or more matches */
        if ($matchedSymbols > 2) {
            $win_amount = $this->calculatePrize($matchedSymbols);

            /* making json object as requested on assignment */
            $obj = new stdClass();
            $auxPayline = join(' ', $payline);
            $obj->$auxPayline = $matchedSymbols;

            /* adds the payline to the output json */
            array_push($this->jsonResult->paylines, $obj);
            /* adds the current amount won from current payline into the output win_amount */
            $this->jsonResult->total_win = $this->jsonResult->total_win + $win_amount;
        }
    }

    protected function printResult() {
        $this->info(json_encode($this->jsonResult));
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->initVariables();
            $this->initBoardSymbols();
            $this->verifyPaylines();
            $this->printResult();
        } catch (Exception $e) {
            $this->error($e);
        }
    }
}