<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use stdClass;
use Exception;

class SlotsGameCommandTest extends SlotsGameCommand {
	/**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'slots:test';

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "slots:test";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Slots game for VideoSlots assignment. using test board ['J', 'J', 'J', 'Q', 'K', 'CAT', 'J', 'Q', 'MONKEY', 'BIRD', 'BIRD', 'BIRD', 'J', 'Q', 'A']";

    protected function initBoardSymbols() {
        $this->boardSymbols = ['J', 'J', 'J', 'Q', 'K', 'CAT', 'J', 'Q', 'MONKEY', 'BIRD', 'BIRD', 'BIRD', 'J', 'Q', 'A'];

        $this->jsonResult->board = $this->boardSymbols;
    }
}
?>